/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package programaif;

import java.util.Scanner;

/**
 *
 * @author Omar Rodriguez
 */
public class Programaif {


    /**
     * Clase      : imprimirMsj
     * Parámetros : String sMensaje
     * Acción     : Imprime el mensaje y hace un salto de línea
     */
    static void imprimirMsj(String sMensaje){
        System.out.println(sMensaje);
    }
    
    /**
     * Clase      : imprimirM
     * Parámetros : String sMen
     * Acción     : Imprime el mensaje en la misma línea 
     */
    static void imprimirM(String sMen){
        System.out.print(sMen);
    }
    
    /**
     * Clase      : separador
     * Parámetros : 
     * Acción     : Crea un separador
     */
    static void separador(){
        imprimirMsj("-------------------------------");
    }
    
     /**
     * Clase      : encabezado
     * Parámetros : 
     * Acción     : Muestra información sobre el programa
     */
    static void encabezado(){
        imprimirMsj("Universidad Autónoma de Campeche");
        imprimirMsj("Facultad de Ingeniería");
        imprimirMsj("Ingeniería en Sistemas Computacionales");
        imprimirMsj("Omar Jasiel Rodriguez Cab | 56964");
        separador();
    }
    
    /**
     * Clase      : diasSemana
     * Parámetros : 
     * Acción     : Imprimir el día de la semana dependiendo del número que se introduzca
     */
    static void diasSemana(){
        Scanner sc = new Scanner(System.in);
        imprimirM("Introduzca un número del 1 al 7: ");
        int iDia = sc.nextInt();
        if (iDia == 1 ){
            imprimirMsj("El día elegido es Domingo");
        }
        else{
            if (iDia == 2){
                imprimirMsj("El día elegido es Lunes");
            }
            else{
                if (iDia == 3){
                    imprimirMsj("El día elegido es Martes");
                }
                else{
                    if (iDia == 4){
                        imprimirMsj("El día elegido es Miércoles");
                    }
                    else{
                        if (iDia == 5){
                            imprimirMsj("El día elegido es Jueves");
                        }
                        else{
                            if (iDia == 6){
                                imprimirMsj("El día elegido es Viernes");
                            }
                            else{
                                if (iDia == 7){
                                    imprimirMsj("El día elegido es Sábado");
                                }
                                else{
                                    if (iDia < 1 || iDia > 7){
                                        imprimirMsj("El número introducido no es válido");
                                    }
                                }
                            }
                        }
                }
                }
            }
        } 
        separador();
    }
    
    /**
     * Clase      : difNum
     * Parámetros : 
     * Acción     : Programa para decir si un número está más cerca de uno o del otro
     */
    static void difNum(){
        Scanner sc = new Scanner(System.in);
        imprimirMsj("Introduzca 3 números");
        imprimirM("Introduzca el primer número: ");
        int iNum1 = sc.nextInt();
        imprimirM("Introduzca el segundo número: ");
        int iNum2 = sc.nextInt();
        imprimirM("Introduzca el tercer número: ");
        int iNum3 = sc.nextInt();
        separador();
        int iOper1 = Math.abs(iNum1 - iNum3);
        int iOper2 = Math.abs(iNum2 - iNum3);
            if (iOper1 > iOper2){
                imprimirMsj("El tercer número está mas cerca del primero");
            }
            else{
                if(iOper1 < iOper2){
                    imprimirMsj("El tercer número está más cerca del segundo");
                }
                else if (iOper1 == iOper2){
                    imprimirMsj("El tercer número está a la misma distancia de los otros dos");
                }
            }
        separador();
    }
    
    /**
     * Clase      : submenu
     * Parámetros : iOpc 
     * Acción     : Arroja el programa que se selecciona en el menú
     */
    static void submenu(int iOpc){
        if (iOpc == 1){
            imprimirMsj("Día de la semana");
            separador();
            diasSemana();
        }
        else{
            if (iOpc == 2){
                imprimirMsj("Diferencia entre números");
                separador();
                difNum();
            }
            else if (iOpc == 3){
                imprimirMsj("Gracias por usar la aplicación!");
                separador();
            }
        }
    }
    
    /**
     * Clase      : menu
     * Parámetros : 
     * Acción     : Escoge un programa de acuerdo al número introducido
     */
    static void menu(){
        imprimirMsj("MENÚ.");
        imprimirMsj("1.- Introducir un número y decir que día de la semana es.");
        imprimirMsj("2.- Introducir 3 números y decir si el tercero está más lejos del primero o del segundo.");
        imprimirMsj("3.- Salir.");
        separador();
        Scanner sc = new Scanner(System.in);
        imprimirM("Teclee la opción deseada: ");
        int iOpc = sc.nextInt();
        submenu(iOpc);
        
    }
    public static void main(String[] args) {
        // TODO code application logic here
        encabezado();
        menu();
    }
    
}
